package com.havenuidea.transfer;

import com.havenuidea.transfer.account.Account;
import com.havenuidea.transfer.account.AccountRepository;
import com.havenuidea.transfer.server.DBHelper;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class ConcurrentTransferTest {

    @Parameters
    public static Collection<Object[]> data() {
        List<Object[]> params = new ArrayList<>();
        params.add(new Object[]{1, 0});
        params.add(new Object[]{10, 0});
        params.add(new Object[]{10, 100});
        params.add(new Object[]{100, 0});
        params.add(new Object[]{100, 100});
        return params;
    }

    private int numberOfThreads;
    private int delay;
    private AccountRepository dao;
    private TransferService service;

    public ConcurrentTransferTest(int numberOfThreads, int delay) {
        this.numberOfThreads = numberOfThreads;
        this.delay = delay;
        dao = AccountRepository.getInstance();
        service = TransferService.getInstance();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        DBHelper.initDb();
    }

    @Test
    public void testConcurrentTransfers() throws InterruptedException {
        Account startW = new Account(UUID.randomUUID().toString(), 10);
        Account startD = new Account(UUID.randomUUID().toString(), 20);
        dao.createAccount(startW);
        dao.createAccount(startD);
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        Runnable run = () -> {
            try {
                Thread.sleep(Math.round(Math.random() * delay));
                service.transfer(startW.getId(), startD.getId(), 1);
            } catch (InterruptedException ignore) {
                //intentionally blank
            } finally {
                latch.countDown();
            }
        };
        IntStream.range(0, numberOfThreads).forEach(i -> executorService.submit(run));
        latch.await(1, TimeUnit.SECONDS);
        Account endW = dao.getAccountById(startW.getId());
        Account endD = dao.getAccountById(startD.getId());
        assertTrue(0 <= endW.getBalance());
        assertTrue(endW.getBalance() < startW.getBalance());
        long delta = startW.getBalance() - endW.getBalance();
        assertEquals(20 + delta, endD.getBalance());
    }
}
