package com.havenuidea.transfer;

import com.havenuidea.transfer.account.Account;
import com.havenuidea.transfer.account.AccountRepository;
import com.havenuidea.transfer.server.DBHelper;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


public class TransferTest {

    private final AccountRepository dao;
    private final TransferService service;

    public TransferTest() {
        dao = AccountRepository.getInstance();
        service = TransferService.getInstance();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        DBHelper.initDb();
    }

    @Test
    public void testTransfer() {
        Account startW = new Account(UUID.randomUUID().toString(), 10);
        Account startD = new Account(UUID.randomUUID().toString(), 20);
        AccountRepository dao = AccountRepository.getInstance();
        dao.createAccount(startW);
        dao.createAccount(startD);
        TransferResultDto result = service.transfer(startW.getId(), startD.getId(), 4);

        long balanceAfterDeposit = result.getDepositedAccount().getBalance();
        assertEquals(20 + 4, balanceAfterDeposit);
        long balanceAfterWithdraw = result.getWithdrawnAccount().getBalance();
        assertEquals(10 - 4, balanceAfterWithdraw);

        Account endW = dao.getAccountById(startW.getId());
        Account endD = dao.getAccountById(startD.getId());
        assertEquals(balanceAfterWithdraw, endW.getBalance());
        assertEquals(balanceAfterDeposit, endD.getBalance());
    }

    @Test
    public void testTransferWithNegativeResult() {
        Account startW = new Account(UUID.randomUUID().toString(), 10);
        Account startD = new Account(UUID.randomUUID().toString(), 20);
        AccountRepository dao = AccountRepository.getInstance();
        dao.createAccount(startW);
        dao.createAccount(startD);
        try {
            service.transfer(startW.getId(), startD.getId(), 15);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("not enough money for transfer", e.getMessage());
        }
        Account endW = dao.getAccountById(startW.getId());
        Account endD = dao.getAccountById(startD.getId());
        assertEquals(startW.getBalance(), endW.getBalance());
        assertEquals(startD.getBalance(), endD.getBalance());
    }

}
