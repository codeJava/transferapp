package com.havenuidea.transfer.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class DBHelper {

    public static final String URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    public static final String USER = "sa";
    public static final String PASSWORD = "";
    private static final String DRIVER = "org.h2.Driver";
    private static final String CREATE_TABLE =
            "create table if not exists ACCOUNTS(id VARCHAR(50) NOT NULL primary key, amount BIGINT NOT NULL)";


    public static void initDb() throws Exception {
        Class.forName(DRIVER).newInstance();
        try (Connection conn = DriverManager.getConnection(DBHelper.URL, DBHelper.USER, DBHelper.PASSWORD);
             PreparedStatement st = conn.prepareStatement(CREATE_TABLE)) {
            st.executeUpdate();
        }
    }
}
