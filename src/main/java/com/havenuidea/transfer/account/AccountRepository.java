package com.havenuidea.transfer.account;

import com.havenuidea.transfer.server.DBHelper;
import org.apache.log4j.Logger;

import java.sql.*;

public class AccountRepository {

    private static Logger logger = Logger.getLogger(AccountRepository.class);

    private static final AccountRepository instance = new AccountRepository();
    private static final String INSERT_QUERY = "insert into ACCOUNTS values(?, ?)";
    private static final String SELECT_QUERY = "select * from ACCOUNTS where id = ?";

    private AccountRepository() {
    }

    public static AccountRepository getInstance() {
        return instance;
    }

    public Account getAccountById(String id) {
        try (Connection conn = DriverManager.getConnection(DBHelper.URL, DBHelper.USER, DBHelper.PASSWORD);
             PreparedStatement ps = conn.prepareStatement(SELECT_QUERY)) {
            ps.setString(1, id);
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                String name = result.getString("id");
                long amount = result.getLong("amount");
                return new Account(name, amount);
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        throw new IllegalArgumentException("no account found for given id");
    }

    public void createAccount(Account a) {
        try (Connection conn = DriverManager.getConnection(DBHelper.URL, DBHelper.USER, DBHelper.PASSWORD);
             PreparedStatement ps = conn.prepareStatement(INSERT_QUERY)) {
            ps.setString(1, a.getId());
            ps.setLong(2, a.getBalance());
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}