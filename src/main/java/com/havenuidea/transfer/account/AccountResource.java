package com.havenuidea.transfer.account;

import com.havenuidea.transfer.message.BalanceResponse;
import com.havenuidea.transfer.message.CreateAccountRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/account/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountResource {

    private AccountRepository dao = AccountRepository.getInstance();

    @POST
    @Path("create/")
    public BalanceResponse createAccount(CreateAccountRequest request) {
        dao.createAccount(new Account(request.accountId, request.balance));
        return new BalanceResponse(request.accountId, request.balance);
    }

    @GET
    @Path("balance/{accountId}")
    public BalanceResponse getBalance(@PathParam("accountId") String accountId) {
        long balance = dao.getAccountById(accountId).getBalance();
        return new BalanceResponse(accountId, balance);
    }

}
