package com.havenuidea.transfer.exception;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DefaultExceptionMapper implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception e) {
        return Response
                .serverError()
                .type(MediaType.APPLICATION_JSON)
                .entity(new ErrorMessage(e.getMessage()))
                .build();
    }

    class ErrorMessage {

        @JsonProperty("message")
        public String message;

        public ErrorMessage(String message) {
            this.message = message;
        }
    }
}
