package com.havenuidea.transfer;

import com.havenuidea.transfer.message.BalanceResponse;
import com.havenuidea.transfer.message.TransferRequest;
import com.havenuidea.transfer.message.TransferResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/transfer/")
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource {

    private TransferService service = TransferService.getInstance();

    @POST
    @Path("")
    public TransferResponse transfer(TransferRequest request) {
        TransferResultDto transfer = service.transfer(request.accountToWithdraw,
                request.accountToDeposit,
                request.transferAmount);
        return new TransferResponse(
                new BalanceResponse(transfer.getWithdrawnAccount()),
                new BalanceResponse(transfer.getDepositedAccount()));
    }

}
