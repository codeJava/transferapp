package com.havenuidea.transfer;

import com.havenuidea.transfer.account.Account;
import com.havenuidea.transfer.server.DBHelper;

import java.sql.*;

public class TransferService {

    private static final TransferService instance = new TransferService();
    private static final String SELECT_TWO_QUERY = "select * from ACCOUNTS where id in (?, ?)";
    private static final String UPDATE_QUERY = "update ACCOUNTS set amount = ? where id = ? and amount = ?";

    private TransferService() {
    }

    public static TransferService getInstance() {
        return instance;
    }

    public TransferResultDto transfer(String accountIdToWithdraw, String accountIdToDeposit, long amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("amount should be more than 0");
        }
        try (Connection conn = DriverManager.getConnection(DBHelper.URL, DBHelper.USER, DBHelper.PASSWORD)) {

            PreparedStatement select = conn.prepareStatement(SELECT_TWO_QUERY);
            select.setString(1, accountIdToWithdraw);
            select.setString(2, accountIdToDeposit);
            ResultSet resultSet = select.executeQuery();
            Account withdrawAccount = null;
            Account depositAccount = null;
            while (resultSet.next()) {
                String accountId = resultSet.getString("id");
                long accountAmount = resultSet.getLong("amount");
                if (accountId.equals(accountIdToWithdraw)) {
                    if (accountAmount < amount) {
                        throw new IllegalStateException("not enough money for transfer");
                    }
                    withdrawAccount = new Account(accountId, accountAmount);
                } else {
                    depositAccount = new Account(accountId, accountAmount);
                }
            }
            if (withdrawAccount == null || depositAccount == null) {
                throw new IllegalStateException("account not found");
            }

            conn.setAutoCommit(false);
            try {
                PreparedStatement withdraw = conn.prepareStatement(UPDATE_QUERY);
                withdraw.setLong(1, withdrawAccount.getBalance() - amount);
                withdraw.setString(2, withdrawAccount.getId());
                withdraw.setLong(3, withdrawAccount.getBalance());
                int withdrawSuccess = withdraw.executeUpdate();
                if (withdrawSuccess != 1) {
                    conn.rollback();
                    throw new IllegalStateException("withdraw update failed");
                }

                PreparedStatement deposit = conn.prepareStatement(UPDATE_QUERY);
                deposit.setLong(1, depositAccount.getBalance() + amount);
                deposit.setString(2, depositAccount.getId());
                deposit.setLong(3, depositAccount.getBalance());
                int depositSuccess = deposit.executeUpdate();
                if (depositSuccess != 1) {
                    conn.rollback();
                    throw new IllegalStateException("deposit update failed");
                }
                conn.commit();
                withdrawAccount = new Account(withdrawAccount.getId(), withdrawAccount.getBalance() - amount);
                depositAccount = new Account(depositAccount.getId(), depositAccount.getBalance() + amount);
                return new TransferResultDto(withdrawAccount, depositAccount);
            } catch (SQLException e) {
                conn.rollback();
                throw new RuntimeException("Transfer failed", e);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Transfer failed", e);
        }
    }
}
