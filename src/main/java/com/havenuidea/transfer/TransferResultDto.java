package com.havenuidea.transfer;

import com.havenuidea.transfer.account.Account;

public class TransferResultDto {

    private Account withdrawnAccount;
    private Account depositedAccount;


    public TransferResultDto(Account withdrawnAccount, Account depositedAccount) {
        this.withdrawnAccount = withdrawnAccount;
        this.depositedAccount = depositedAccount;
    }

    public Account getWithdrawnAccount() {
        return withdrawnAccount;
    }

    public Account getDepositedAccount() {
        return depositedAccount;
    }
}
