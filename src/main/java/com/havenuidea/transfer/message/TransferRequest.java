package com.havenuidea.transfer.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TransferRequest {

    public String accountToWithdraw;
    public String accountToDeposit;
    public long transferAmount;

    public TransferRequest() {
    }

    @JsonCreator
    public TransferRequest(
            @JsonProperty("accountToWithdraw") String accountToWithdraw,
            @JsonProperty("accountToDeposit") String accountToDeposit,
            @JsonProperty("transferAmount") long transferAmount) {
        this.accountToWithdraw = accountToWithdraw;
        this.accountToDeposit = accountToDeposit;
        this.transferAmount = transferAmount;
    }

}
