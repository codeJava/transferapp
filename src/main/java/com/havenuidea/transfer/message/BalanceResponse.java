package com.havenuidea.transfer.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.havenuidea.transfer.account.Account;

public class BalanceResponse {

    @JsonProperty("accountId")
    public String accountId;
    @JsonProperty("balance")
    public long balance;

    public BalanceResponse() {
    }

    @JsonCreator
    public BalanceResponse(
            @JsonProperty("accountId") String accountId,
            @JsonProperty("balance") long balance) {
        this.accountId = accountId;
        this.balance = balance;
    }

    public BalanceResponse(Account a) {
        this.accountId = a.getId();
        this.balance = a.getBalance();
    }
}
