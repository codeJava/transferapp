package com.havenuidea.transfer.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TransferResponse {

    @JsonProperty("accountToWithdraw")
    public BalanceResponse accountToWithdraw;
    @JsonProperty("accountToDeposit")
    public BalanceResponse accountToDeposit;

    public TransferResponse() {
    }

    @JsonCreator
    public TransferResponse(
            @JsonProperty("accountToWithdraw") BalanceResponse accountToWithdraw,
            @JsonProperty("accountToDeposit") BalanceResponse accountToDeposit) {
        this.accountToWithdraw = accountToWithdraw;
        this.accountToDeposit = accountToDeposit;
    }
}
