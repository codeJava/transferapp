package com.havenuidea.transfer.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateAccountRequest {

    public String accountId;
    public long balance;

    public CreateAccountRequest() {
    }

    @JsonCreator
    public CreateAccountRequest(
            @JsonProperty("accountId") String accountId,
            @JsonProperty("balance") long balance) {
        this.accountId = accountId;
        this.balance = balance;
    }

}
